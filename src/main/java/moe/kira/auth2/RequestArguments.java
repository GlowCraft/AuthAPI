package moe.kira.auth2;

import java.net.Proxy;

import javax.annotation.Nullable;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class RequestArguments {
    private @Nullable final Proxy connectionProxy;
    
    private final String authServerUrl;
    private final String httpContentType;
    
    private final int connectTimeout;
    private final int readTimeout;
}
