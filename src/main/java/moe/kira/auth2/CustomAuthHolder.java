package moe.kira.auth2;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;

import com.google.common.base.Charsets;
import lombok.NonNull;
import moe.kira.auth2.AuthAPI.AuthCallback;
import moe.kira.auth2.AuthAPI.AuthHolder;
import moe.kira.auth2.AuthAPI.AuthResult;

import static moe.kira.auth2.AuthAPI.Constans;
import static moe.kira.auth2.AuthAPI.runCallback;
import static moe.kira.auth2.AuthAPI.parser;

import static moe.kira.auth2.AuthAPIBoostrap.instance;
import static moe.kira.auth2.AuthAPIBoostrap.scheduler;

@ThreadSafe
public class CustomAuthHolder implements AuthHolder {
    final RequestArguments args;
    
    public CustomAuthHolder(@NonNull RequestArguments arguments) {
        args = arguments;
    }
    
    @Override
    public void postAuth(@Nullable final String username, @NonNull final String email, @NonNull final String password, @NonNull final AuthCallback callback) {
        scheduler.runTaskAsynchronously(instance, new Runnable() {
            @Override
            public void run() {
                try {
                    doAuth(username, email, password, callback);
                } catch (Throwable t) {
                    runCallback(AuthResult.ERROR, t, callback);
                }
            }
        });
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public AuthResult doAuth(@Nullable String username, @NonNull String email, @NonNull String password, @Nullable AuthCallback callback) throws Throwable {
        try {
            HttpURLConnection conn = AuthAPI.createConnection(args);
            conn.connect();
            DataOutputStream out = new DataOutputStream(conn.getOutputStream());
            
            JSONObject userInfo = new JSONObject();
            userInfo.put(Constans.STRING_USERNAME, email);
            userInfo.put(Constans.STRING_PASSWORD, password);
            
            JSONObject agent = new JSONObject();
            agent.put(Constans.STRING_NAME, Constans.MOJANG_AGENT_TYPE);
            agent.put(Constans.STRING_VERSION, Constans.MOJANG_AGENT_VERSION);
            
            userInfo.put(Constans.STRING_AGENT, agent);
            out.writeBytes(userInfo.toString());
            out.flush();
            out.close();
            
            // match code
            if (conn.getResponseCode() == 403) {
                runCallback(AuthResult.DENY, null, callback);
                return AuthResult.DENY;
            }
            if (conn.getResponseCode() != 200) {
                runCallback(AuthResult.UNKNOWN, null, callback);
                return AuthResult.UNKNOWN;
            }
            
            // match username
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder builder = new StringBuilder("");
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(new String(line.getBytes(), Charsets.UTF_8));
            }
            JSONObject data = (JSONObject) parser.parse(builder.toString());
            
            Object selectedProfile = data.get(Constans.STRING_SELECTED_PROFILE);
            if (selectedProfile == null) {
                runCallback(AuthResult.DENY, null, callback);
                return AuthResult.DENY;
            }
            JSONObject profile = (JSONObject) parser.parse(selectedProfile.toString());
            
            if (StringUtils.isBlank(username) || profile.get(Constans.STRING_NAME).equals(username)) {
                runCallback(AuthResult.SUCCESS, null, callback);
                return AuthResult.SUCCESS;
            } else {
                runCallback(AuthResult.SUCCESS_RENT, null, callback);
                return AuthResult.SUCCESS_RENT;
            }
        } catch (Throwable t) {
            throw t;
        }
    }
    
}
