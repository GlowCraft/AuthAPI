package moe.kira.auth2;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;

import org.bukkit.Bukkit;
import org.json.simple.parser.JSONParser;

import lombok.NonNull;
import static moe.kira.auth2.AuthAPIBoostrap.instance;
import static moe.kira.auth2.AuthAPIBoostrap.scheduler;

public class AuthAPI {
    public final static JSONParser parser = new JSONParser();
    
    /**
     * Creates a Mojang Yggdrasil auth holder
     * @return {@link MojangAuthHolder}
     */
    public static MojangAuthHolder create() {
        return new MojangAuthHolder();
    }
    
    /**
     * Creates a custom Yggdrasil auth holder with given connection arguments
     * @return {@link CustomAuthHolder}
     */
    public static CustomAuthHolder create(@NonNull RequestArguments arguments) {
        return new CustomAuthHolder(arguments);
    }
    
    /**
     * Runs a callback
     * @param result {@link AuthResult}
     * @param t exception
     * @param callback
     */
    @StrictAsync
    public static void runCallback(final AuthResult result, @Nullable final Throwable t, @Nullable final AuthCallback callback) {
        if (callback == null) return;
        
        if (callback instanceof SyncCallback && !Bukkit.isPrimaryThread()) {
            scheduler.runTask(instance, new Runnable() {
                @Override
                public void run() {
                    callback.run(result, t);
                }
            });
        } else {
            callback.run(result, t);
        }
    }
    
    /**
     * Simply creates a connection based on given arguments
     * @param args connection arguments
     * @return HttpURLConnection
     * @throws IOException
     */
    public static HttpURLConnection createConnection(@NonNull RequestArguments args) throws IOException {
        URL url = new URL(args.getAuthServerUrl());
        HttpURLConnection conn = (HttpURLConnection) (args.getConnectionProxy() == null ? url.openConnection() : url.openConnection(args.getConnectionProxy()));
        
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setUseCaches(false);
        conn.setInstanceFollowRedirects(true);
        int timeout = args.getConnectTimeout() > 0 ? args.getConnectTimeout() : Constans.DEFAULT_TIMEOUT_MILLIS;
        conn.setConnectTimeout(timeout);
        conn.setReadTimeout(timeout);
        conn.setRequestProperty(Constans.CONTENT_TYPE, args.getHttpContentType() == null ? Constans.DEFAULT_CONTENT_TYPE_VALUE : args.getHttpContentType());
        conn.setRequestMethod(Constans.REQUEST_METHOD_POST);
        
        return conn;
    }
    
    public static enum AuthResult {
        SUCCESS,
        /**
         * Account verified but the username was mismatched
         */
        SUCCESS_RENT,
        DENY,
        ERROR,
        /**
         * They responded an unexpected response code
         */
        UNKNOWN;
    }
    
    public @interface Async {}
    public @interface StrictAsync {}
    
    @FunctionalInterface
    public interface AuthCallback {
        /**
         * @throws IOException
         * @throws UnknownHostException
         * @throws ParseException
         */
        void run(AuthResult result, Throwable t);
    }
    
    @FunctionalInterface
    public interface SyncCallback extends AuthCallback {
        /**
         * Ensures it will be run in the primary thread
         * @throws IOException
         * @throws UnknownHostException
         * @throws ParseException
         */
        @Override void run(AuthResult result, Throwable t);
    }
    
    public interface AuthHolder {
        /**
         * Posts an auth asynchronously
         * @param username
         * @param email
         * @param password
         * @param callback
         */
        @Async
        void postAuth(@Nullable String username, String email, String password, @NonNull AuthCallback callback);
        
        /**
         * @param username
         * @param email
         * @param password
         * @return {@link AuthResult}
         * @throws IOException
         * @throws UnknownHostException
         * @throws ParseException
         */
        AuthResult doAuth(@Nullable String username, String email, String password, @Nullable AuthCallback callback) throws Throwable;
    }
    
    public static class Constans {
        static final String CONTENT_TYPE = "Content-Type";
        static final String DEFAULT_CONTENT_TYPE_VALUE = "application/json; charset=utf-8";
        static final String REQUEST_METHOD_POST = "POST";
        static final int DEFAULT_TIMEOUT_MILLIS = (int) TimeUnit.SECONDS.toMillis(15);
        
        static final String MOJANG_AUTH_SERVER = "https://authserver.mojang.com/authenticate";
        static final String MOJANG_AGENT_TYPE = "minecraft";
        static final int MOJANG_AGENT_VERSION = 1;
        
        static final String STRING_SELECTED_PROFILE = "selectedProfile";
        static final String STRING_USERNAME = "username";
        static final String STRING_PASSWORD = "password";
        static final String STRING_VERSION = "version";
        static final String STRING_AGENT = "agent";
        static final String STRING_NAME = "name";
    }
}
