package moe.kira.auth2;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

public class AuthAPIBoostrap extends JavaPlugin {
    static AuthAPIBoostrap instance;
    static BukkitScheduler scheduler;
    
    @Override
    public void onEnable() {
        instance = this;
        scheduler = Bukkit.getScheduler();
    }
}
